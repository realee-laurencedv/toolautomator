# HardWare Auto-Test Tool

## Brief

[HardWare Auto-Test Tool][home_link] (HWATT pronounced _h'what_) is a python software to automate testing with electronics lab equipment.
It support multiple of-the-shelf equipment and custom ones. See the [currently supported list](/doc/tool_supported.md) for more detail.

See the [roadmap](/doc/roadmap.md) for planned development

## Contrib

### Development setup

We assume [poetry](https://python-poetry.org/docs/) & [pyenv](https://github.com/pyenv/pyenv) is already [installed](https://laurencedv.org/computing/python) locally
Open the **terminal** in VSCode (also work directly in OS) then follow those instructions:

1. Install venv with poetry locally

   ```bash
   poetry install
   ```

2. Select python interpreter:  
   `CTRL` + `SHIFT` + `P` , then type _python: select interpreter_ and select the newly installed venv.
   > If you don't see it, you can use the _enter path_ option and use `poetry env info --path` in the terminal to find the path

### Generating executable

All command are regroupped into the [pyinstaller.py](./hwatt/pyinstaller.py) file.

1. Run pyinstaller within the venv

   ```bash
   poetry run build
   ```

2. The executable should be in the `dist/` folder
3. Clean-up

   ```bash
   poetry run clean
   ```

## Known problem

### USB instrument need udev rights

To use a USB instrument on a linux computer, you will probably need to add some udev rules. There is a [file](doc/51-usbtmc.rules) containing rules for some intruments, move this file in the folder `/etc/udev/rules.d/` then call `sudo udevadm control --reload-rules` to apply them.

> You can also use `sudo udevadm info --name=/dev/bus/usb/xxx/yyy` to have more info on the connected tools.  
> Use `lsusb` to find the correct _xxx_ _yyy_ values

### Serial port access rights

Ensure your UNIX user is in the _plugdev_ and/or _dialout_ group to have access to serial ports. See [online guide](https://laurencedv.org/en/computing/linuxmint/usermng#add-groups-to-current-user) to do that

## Changelog

See [release.md](/doc/release.md) file

[home_link]: https://gitlab.com/real-ee/public/hwatt
