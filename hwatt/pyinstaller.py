import os
import shutil
import PyInstaller.__main__
from pathlib import Path

HERE = Path(__file__).parent.absolute()
path_to_main = str(HERE / "./hwatt.py")
path_to_add = "./hwatt"
hidden_import = "pyvisa_py"
build_paths = [str(HERE / "../build/"), str(HERE / "../dist/"), str(HERE / "../hwatt.spec"),]
log_path = "./log"


def install():
    PyInstaller.__main__.run([
        path_to_main,
        '--onefile',
        '--paths=' + path_to_add,
        '--hidden-import=' + hidden_import,
        # '--debug'
        # other pyinstaller options...
    ])


def clean():
    try:
        for path in build_paths:
            if os.path.isfile(path):
                os.remove(path)
                print(f"File '{path}' removed successfully.")
            elif os.path.isdir(path):
                shutil.rmtree(path)
                print(f"Directory '{path}' removed successfully.")
            else:
                print(f"'{path}' is neither a file nor a directory.")

    except Exception as e:
        print(f"An error occurred: {e}")


def clean_log():
    try:
        if os.path.isfile(log_path):
            os.remove(log_path)
            print(f"File '{log_path}' removed successfully.")
        elif os.path.isdir(log_path):
            shutil.rmtree(log_path)
            print(f"Directory '{log_path}' removed successfully.")
        else:
            print(f"'{log_path}' is neither a file nor a directory.")

    except Exception as e:
        print(f"An error occurred: {e}")
