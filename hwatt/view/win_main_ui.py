# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'win_main.ui'
##
## Created by: Qt User Interface Compiler version 6.5.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QAbstractItemView, QApplication, QFrame, QGridLayout,
    QHBoxLayout, QHeaderView, QMainWindow, QMdiArea,
    QMenu, QMenuBar, QSizePolicy, QStatusBar,
    QTableWidget, QTableWidgetItem, QVBoxLayout, QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 633)
        self.actionSave_current_session = QAction(MainWindow)
        self.actionSave_current_session.setObjectName(u"actionSave_current_session")
        self.actionLoad_session = QAction(MainWindow)
        self.actionLoad_session.setObjectName(u"actionLoad_session")
        self.actionAdd_a_tool = QAction(MainWindow)
        self.actionAdd_a_tool.setObjectName(u"actionAdd_a_tool")
        self.actionAdd_a_step = QAction(MainWindow)
        self.actionAdd_a_step.setObjectName(u"actionAdd_a_step")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.mdiArea = QMdiArea(self.centralwidget)
        self.mdiArea.setObjectName(u"mdiArea")
        self.mdiArea.setFrameShape(QFrame.NoFrame)
        self.mdiArea.setViewMode(QMdiArea.TabbedView)
        self.mdiArea.setDocumentMode(True)
        self.mdiArea.setTabsMovable(False)
        self.subwindow_tools = QWidget()
        self.subwindow_tools.setObjectName(u"subwindow_tools")
        self.horizontalLayout_2 = QHBoxLayout(self.subwindow_tools)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")

        self.horizontalLayout_2.addLayout(self.gridLayout)

        self.mdiArea.addSubWindow(self.subwindow_tools)
        self.subwindow_steps = QWidget()
        self.subwindow_steps.setObjectName(u"subwindow_steps")
        self.horizontalLayout = QHBoxLayout(self.subwindow_steps)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.table_steplist = QTableWidget(self.subwindow_steps)
        if (self.table_steplist.columnCount() < 4):
            self.table_steplist.setColumnCount(4)
        __qtablewidgetitem = QTableWidgetItem()
        self.table_steplist.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.table_steplist.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.table_steplist.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.table_steplist.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        if (self.table_steplist.rowCount() < 1):
            self.table_steplist.setRowCount(1)
        self.table_steplist.setObjectName(u"table_steplist")
        self.table_steplist.setDragDropMode(QAbstractItemView.InternalMove)
        self.table_steplist.setAlternatingRowColors(True)
        self.table_steplist.setRowCount(1)
        self.table_steplist.setColumnCount(4)
        self.table_steplist.horizontalHeader().setCascadingSectionResizes(False)
        self.table_steplist.horizontalHeader().setStretchLastSection(True)

        self.horizontalLayout.addWidget(self.table_steplist)

        self.mdiArea.addSubWindow(self.subwindow_steps)

        self.verticalLayout.addWidget(self.mdiArea)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 800, 22))
        self.menuFile = QMenu(self.menubar)
        self.menuFile.setObjectName(u"menuFile")
        self.menuSession = QMenu(self.menubar)
        self.menuSession.setObjectName(u"menuSession")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuSession.menuAction())
        self.menuFile.addAction(self.actionSave_current_session)
        self.menuFile.addAction(self.actionLoad_session)
        self.menuFile.addSeparator()
        self.menuSession.addAction(self.actionAdd_a_tool)
        self.menuSession.addSeparator()
        self.menuSession.addAction(self.actionAdd_a_step)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Tool Automator", None))
        self.actionSave_current_session.setText(QCoreApplication.translate("MainWindow", u"Save session", None))
        self.actionLoad_session.setText(QCoreApplication.translate("MainWindow", u"Load session", None))
        self.actionAdd_a_tool.setText(QCoreApplication.translate("MainWindow", u"Add a tool", None))
        self.actionAdd_a_step.setText(QCoreApplication.translate("MainWindow", u"Add a step", None))
        self.subwindow_tools.setWindowTitle(QCoreApplication.translate("MainWindow", u"Tools", None))
        self.subwindow_steps.setWindowTitle(QCoreApplication.translate("MainWindow", u"Steps", None))
        ___qtablewidgetitem = self.table_steplist.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("MainWindow", u"Time", None));
        ___qtablewidgetitem1 = self.table_steplist.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("MainWindow", u"Tool", None));
        ___qtablewidgetitem2 = self.table_steplist.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("MainWindow", u"Action", None));
        ___qtablewidgetitem3 = self.table_steplist.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("MainWindow", u"Value", None));
        self.menuFile.setTitle(QCoreApplication.translate("MainWindow", u"File", None))
        self.menuSession.setTitle(QCoreApplication.translate("MainWindow", u"Session", None))
    # retranslateUi

