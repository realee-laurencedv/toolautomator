# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'tool_dp700_debug.ui'
##
## Created by: Qt User Interface Compiler version 6.5.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QHBoxLayout, QLineEdit, QPushButton,
    QSizePolicy, QTextBrowser, QTextEdit, QVBoxLayout,
    QWidget)

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(379, 274)
        self.verticalLayout_2 = QVBoxLayout(Form)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.btn_send = QPushButton(Form)
        self.btn_send.setObjectName(u"btn_send")

        self.horizontalLayout.addWidget(self.btn_send)

        self.lineEdit_msg = QLineEdit(Form)
        self.lineEdit_msg.setObjectName(u"lineEdit_msg")
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_msg.sizePolicy().hasHeightForWidth())
        self.lineEdit_msg.setSizePolicy(sizePolicy)
        self.lineEdit_msg.setMinimumSize(QSize(100, 0))

        self.horizontalLayout.addWidget(self.lineEdit_msg)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.textBrowser_log = QTextBrowser(Form)
        self.textBrowser_log.setObjectName(u"textBrowser_log")
        self.textBrowser_log.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.textBrowser_log.setLineWrapMode(QTextEdit.NoWrap)
        self.textBrowser_log.setAcceptRichText(False)

        self.verticalLayout.addWidget(self.textBrowser_log)


        self.verticalLayout_2.addLayout(self.verticalLayout)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.btn_send.setText(QCoreApplication.translate("Form", u"Send", None))
        self.textBrowser_log.setDocumentTitle(QCoreApplication.translate("Form", u"log", None))
    # retranslateUi

