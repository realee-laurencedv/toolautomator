import logging

import pyvisa as visa
import serial


class MngInst:

    _inst_list_ = []
    _inst_nb_ = 0

    # instance attributes
    def __init__(self, logger):
        self.logger = logging.getLogger('instrument mng')
        self.logger.setLevel(logger.level)
        self.logger.handlers = logger.handlers

        self.logger.debug(f'created')

        # region VISA ressources manager
        self.visarm = visa.ResourceManager('@py')
        self.logger.debug(f"VISA ressource manager created with backend: {self.visarm.visalib.library_path}")
        # endregion

    def detect_inst(self):
        """Detect all supported tool
        """
        self.logger.debug(f'detection started')
        self._inst_list_ = self.visarm.list_resources()
        self._inst_nb_ = len(self._inst_list_)
        self.logger.info(f"detected {self._inst_nb_} instruments: {', '.join(self._inst_list_)}")

    def get_all_inst(self):
        """Return the complete list of detected instruments
        """

        return self._inst_list_

    def inst_query(self, inst, query, delay=0.5):
        """Abstracted inst query method

        Args:
            inst (inst_type): Tool to send the query to
            query (string): Actual query formatted in a string
        """
        if (inst is not None) and (query is not None):
            try:
                self.logger.debug(f'trying to query {query} to {inst} with {delay}s timeout')
                inst.query(str(query), delay=delay)
            except visa.VisaIOError:
                self.logger.error(f'Failed query {query} to {inst}')


# FROM OLD RUN METHOD: (TODO remove this asap)
# dp712 = self.visarm.open_resource(self.toolList[2])
# dl3601 = self.visarm.open_resource(self.toolList[4])
# ds1000z = self.visarm.open_resource(self.toolList[3])
# dp712.read_termination = '\n'
# dp712.write_termination = '\n'

# self.toolQuery(ds1000z, ":STOP")
# time.sleep(0.1)
# self.toolQuery(ds1000z, ":RUN")
# time.sleep(0.1)

# self.toolQuery(dl3601, "DEB:KEY ON")
# self.toolQuery(dl3601, "SYST:KEY 9")
# self.toolQuery(dl3601, "SYST:KEY 9")
# self.toolQuery(dl3601, "SYST:KEY 9")
# self.toolQuery(dl3601, "SYST:KEY 9")

# self.toolQuery(dp712, "SYST:BEEP:IMM")

# dp712.close()
# dl3601.close()
# ds1000z.close()
