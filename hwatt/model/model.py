import logging
from logging.handlers import QueueHandler
from enum import Enum
from time import sleep
from multiprocessing import Process, Queue, current_process
from queue import Empty

from ipc import IpcMsg, IpcMsgType
from model.mng_sess import MngSess
from model.mng_inst import MngInst


class Model:
    """ HWATT top-level model object containing all business-logic
    """

    def __init__(self, verb, q_log, q_rx, q_tx):
        """ Creator
        """
        super(Model, self).__init__()

        self.logger = logging.getLogger('model')
        self.logger.setLevel(verb)
        self.logger.addHandler(QueueHandler(q_log))

        self.q_rx = q_rx
        self.q_tx = q_tx

        self.session = MngSess(logger=self.logger)
        self.instruments = MngInst(logger=self.logger)

        self.logger.debug(f'created q_tx:{self.q_tx}, q_rx:{self.q_rx}')

    def process(self):
        self.logger.info(f'started pid: {current_process().pid}')

        # execution loop
        while True:
            try:
                msg = self.q_rx.get(timeout=0.01)
            except Empty:
                continue

            # Stop execution
            if msg is None:
                break
            # Detect tools
            else:
                self.logger.debug(f'received msg: {msg}')
                if msg.type == IpcMsgType.detect_tools:
                    self.instruments.detect_inst()
                    self.q_tx.put(IpcMsg(IpcMsgType.tools_list, data=self.instruments.get_all_inst()))
                else:
                    self.logger.warning(f'unrecognized msg: {msg}')

        # execution stopping
        self.logger.info(f'stopped pid: {current_process().pid}')
