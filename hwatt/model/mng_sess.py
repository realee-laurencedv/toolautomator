import yaml
import logging


class MngSess:
    """Session manager object"""
    session_file = None      # current session save/load file
    current = None          # current session

    def __init__(self, logger):
        super(MngSess, self).__init__()

        self.logger = logging.getLogger('session mng')
        self.logger.setLevel(logger.level)
        self.logger.handlers = logger.handlers

        self.logger.debug(f'created')

    def save_current_to_file(self, file):
        """Save session file to nv storage"""
        if file is not None:
            try:
                self.logger.debug(f'saving to: {file}')
                with open(file, 'w', encoding="utf-8") as ymlfile:
                    yaml.dump(self.get_current_session(), ymlfile)
                    self.logger.info(f'saved')
            except Exception as ex:
                raise ex

    def load_current_from_file(self, file):
        """Load session file from nv storage"""
        if file is not None:
            try:
                self.logger.debug(f'loading from: {file}')
                with open(file, 'r', encoding="utf-8") as ymlfile:
                    self.current = yaml.load(ymlfile)
                    self.logger.info(f'loaded')
            except Exception as ex:
                raise ex

    def get_current_session(self):
        """return the opened session object"""
        return {'allo': 'mon',
                'ti': 'coco'}
