"""
Currently, this is a living experiment to build a simple framework to verify
hw specification and measures with hw tools

usb device in linux probably needs udev rules file (see https://gitlab.com/real-ee/products/tools/ds1kz-automator/-/blob/master/permission/64-REE.rules)

pip package required:
- pyvisa
- pyvisa-py
- pyusb
- pyserial

possibly needed os package (LM19.3):
- libusb-1.0.0
- tqdm

useful guide for multiprocessing: https://superfastpython.com/multiprocessing-in-python/
"""

# TODO look into multiprocessing.freeze_support() when at packaging stage https://superfastpython.com/multiprocessing-freeze-support-in-python

import sys
import os
import argparse
import time
from time import sleep
from enum import Enum
from multiprocessing import Process, Queue, current_process
from queue import Empty
from logging.handlers import TimedRotatingFileHandler, QueueHandler
import logging

# Custom stuff
from ipc import IpcMsg, IpcMsgType
from ctler import Ctler
from model.model import Model
from view.gui import View


class Hwatt:
    """ HWATT main object, serve as temporary/limited view object if gui is not invoked
    """
    args = None

    # Private constants
    __verb_levels__ = [logging.CRITICAL, logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG]

    def __init__(self, arg):
        super(Hwatt, self).__init__()
        self.arg = arg

        # region Arguments
        arg_parse = argparse.ArgumentParser()
        arg_parse.add_argument("-s", "--session", help="Session file to load/save", type=str, default="default-session.yaml")
        arg_parse.add_argument("-g", "--gui", help="Invoke the Graphical UI", action="store_true", default=False)
        arg_parse.add_argument("-v", "--verbose", help="Increase the verbosity level (can be used upto 4 times)", action='count', default=0)
        arg_parse.add_argument("-d", '--detect', help='Print all detected instruments attached to this computer', action="store_true", default=False)

        self.args = arg_parse.parse_args()  # Parse & save all arguments
        # endregion

        # region Logger
        self.verbosity = self.__verb_levels__[min(self.args.verbose, len(self.__verb_levels__) - 1)]
        self.q_log = Queue()

        # main object logger
        self.logger = logging.getLogger('hwatt')
        self.logger.setLevel(self.verbosity)
        self.logger.addHandler(QueueHandler(self.q_log))

        logger_p = Process(target=self.logger_process, args=(self.q_log,))
        logger_p.start()
        # endregion

        # region Spawn child objects
        # ipc queues
        self.q_m = Queue()
        self.q_v = Queue()
        self.q_c = Queue()

        # model
        self.model = Model(verb=self.verbosity, q_log=self.q_log, q_rx=self.q_m, q_tx=self.q_c)

        # view - gui
        if self.args.gui is True:
            self.view = View(verb=self.verbosity, q_log=self.q_log, q_rx=self.q_v, q_tx=self.q_c)
        # view - cli, still needs ipc queues
        else:
            self.view = self
            self.logger.debug(f'cli view q_tx:{self.q_v}, q_rx:{self.q_c}')

        # controller
        self.ctler = Ctler(verb=self.verbosity, q_log=self.q_log, q_rx=self.q_c, q_v=self.q_v, q_m=self.q_m)
        # endregion

    def run(self):
        """Main execution method

        Returns:
            int: execution status code (POSIX convention)
        """
        self.logger.info(f'started pid: {current_process().pid}')

        status = 0      # execution status

        # region ---- Start child processes ----
        model_process = Process(target=self.model.process, daemon=True)
        model_process.start()
        ctler_process = Process(target=self.ctler.process, daemon=True)
        ctler_process.start()
        # endregion ----------------------------

        if self.args.detect is True:
            self.q_c.put(IpcMsg(IpcMsgType.detect_tools))

            try:
                response = IpcMsg(self.q_v.get(timeout=10))
                if response.type == IpcMsgType.tools_list:
                    print(f'Found {len(response.data)} instruments: {response.data}', flush=True)
            except Empty:
                self.logger.error(f'detection timed-out')
                status = -1

        # region ---- gui process ----
        if self.args.gui is True:
            view_process = Process(target=self.view.process)
            view_process.start()
            status = view_process.join()
        # endregion ------------------

        # end of execution, gracefully close everything
        self.q_m.put(None)
        self.q_c.put(None)
        sleep(0.5)  # give time to other thread to close
        self.logger.info(f'stopped pid: {current_process().pid}')
        self.q_log.put(None)        # stop the logging process
        return status

    def logger_process(self, queue):
        """ logging isolated process

        Args:
            queue (Queue): logging queue receiving all logs to be saved
        """

        logger = logging.getLogger('logger')
        logger.setLevel(self.verbosity)

        # Create a separate folder for logs if it doesn't exist
        log_dir = 'log'
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        # Formater
        formatter = logging.Formatter('%(asctime)s.%(msecs)03d | %(name)-15s | %(levelname)-8s | %(message)s', datefmt='%Y-%m-%dT%H:%M:%S')

        # File logging
        log_filename = os.path.join(log_dir, f'hwatt_{time.strftime("%Y-%m-%d_%H-%M-%S")}.log')
        file_handler = TimedRotatingFileHandler(log_filename, when='midnight', backupCount=4)
        file_handler.setFormatter(formatter)

        # Console logging
        stream_handler = logging.StreamHandler(sys.stdout)
        stream_handler.setLevel(self.verbosity)  # Set the level as needed
        stream_handler.setFormatter(formatter)

        logger.addHandler(file_handler)
        logger.addHandler(stream_handler)

        logger.info(f'started pid: {current_process().pid}')

        # process loop
        while True:
            msg = queue.get()       # pull new message from queue
            if msg is None:         # exit process on special None message
                break
            logger.handle(msg)      # log the message

        logger.info(f'stopped pid: {current_process().pid}')


if __name__ == '__main__':
    obj = Hwatt(sys.argv)
    sys.exit(obj.run())
