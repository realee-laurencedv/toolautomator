# HWATT roadmap

## 0.1.0

- [x] Python executable packaging
- [x] Multiprocess MVC
- [x] Multiprocess logging
- [x] CLI basic command infrastructure

## 0.2.0

- [x] pyVISA run-time environment for equipment support
- [x] Deploy instruction/packaging
- [ ] tab-based main GUI
- [ ] basic menu action slot/signal
- [ ] Session load/save functionality

## 0.3.0

- [ ] Instrument definition syntax
- [ ] Instrument definition parser
- [ ] Instrument display

## 0.4.0

- [ ] Measurement-run syntax definition
- [ ] Measurement-run load/save functionality
- [ ] Measurement-run file parser/sanitizer

## 0.5.0

- [ ] pySerial equipment support
- [ ] Connected tool detector

## 0.6.0

- [ ] Measurement-run nodal view
- [ ] Measurement-run nodal2file converter

## 0.9.0

- [ ] Rigol DP7xx control interfaces
- [ ] Rigol DP7xx command controller
- [ ] Rigol DL3xxx control interfaces
- [ ] Rigol DL3xxx command controller
- [ ] Rigol MSO5xxx control interfaces
- [ ] Rigol MSO5xxx command controller

## 1.0.0

- [ ] Stable execution

## 1.1.0

- [ ] pyUSB HID custom equipment support
- [ ] Test runner setup
