# HWATT releases

See what is planned in the [roadmap][roadmap_file]

## 0.1.0

Release date: _2023-10-04_

**Features:**

- Python executable packaging
- Multiprocess MVC
- Multiprocess logging
- CLI basic command infrastructure

**Known problems:**

- Nothing useful is doable yet

---

[roadmap_file]: roadmap.md
